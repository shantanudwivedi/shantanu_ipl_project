/**
 * Find the number of times each team won the toss and also won the match
 * @param {Object} matchesData - Matches Data
 * @return {Object} - The number of times each team won the toss and also won the match.
 */
function teamsWhichWonTossAndMatch(matchesData) {
    return matchesData.reduce((accumulator, currentValue) => {
        let teamWhoWonToss = currentValue.toss_winner;
        let teamWhoWonMatch = currentValue.winner;
        if (teamWhoWonToss === teamWhoWonMatch) {
            if (accumulator.hasOwnProperty(teamWhoWonToss)) {
                accumulator[teamWhoWonToss] += 1;
            } else {
                accumulator[teamWhoWonToss] = 1;
            }
        }
        return accumulator;
    }, {});
}

module.exports = teamsWhichWonTossAndMatch;