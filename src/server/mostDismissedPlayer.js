/**
 * Find the highest number of times one player has been dismissed by another player.
 * @param {Object} deliveriesData - Deliveries Data
 * @return {Object} - The highest number of times one player has been dismissed by another player
 */
function mostDismissedPlayer(deliveriesData) {
    return deliveriesData.reduce((accumulator, currentValue) => {
        if (currentValue.player_dismissed) {
            let playerDismissed = currentValue.player_dismissed;
            let dismissedBy = currentValue.fielder;
            if (accumulator.hasOwnProperty(playerDismissed)) {
                if (accumulator[playerDismissed].hasOwnProperty(dismissedBy)) {
                    accumulator[playerDismissed][dismissedBy]["dismissalCount"] += 1;
                } else {
                    accumulator[playerDismissed][dismissedBy] = {};
                    accumulator[playerDismissed][dismissedBy]["dismissalCount"] = 1;
                }
            } else {
                accumulator[playerDismissed] = {};
                accumulator[playerDismissed][dismissedBy] = {};
                accumulator[playerDismissed][dismissedBy]["dismissalCount"] = 1;
            }
        }
        return accumulator;
    }, {});
}

module.exports = mostDismissedPlayer;