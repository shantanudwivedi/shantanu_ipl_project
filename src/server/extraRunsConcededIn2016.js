/**
 * Calculate the extra runs conceded per team in the year 2016
 * @param {Object} matchesData - Matches Data
 * @param {Object} deliveriesData - Deliveries Data
 * @returns {Object} - Extra runs conceded per team in the year 2016
 */
function extraRunsConcededIn2016(matchesData, deliveriesData) {
    let matchIds = matchesData
        .filter(function (match) {
            return parseInt(match.season) === 2016
        })
        .map(match => parseInt(match.id));
    let extraRunsPerTeams = {};
    matchIds.filter((matchId) => {
        extraRunsPerTeams = deliveriesData.reduce((accumulator, currentValue) => {
            let battingTeam = currentValue.bowling_team
            if (parseInt(matchId) === parseInt(currentValue.match_id)) {
                if (accumulator.hasOwnProperty(battingTeam)) {
                    accumulator[battingTeam] += parseInt(currentValue.extra_runs)
                } else {
                    accumulator[battingTeam] = parseInt(currentValue.extra_runs)
                }
            }
            return accumulator;
        }, extraRunsPerTeams);
    });
    return extraRunsPerTeams;
}

module.exports = extraRunsConcededIn2016;