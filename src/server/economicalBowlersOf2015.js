/**
 * Calculate the top 10 economical bowlers in the year 2015.
 * @param {Object} matchesData - Matches Data
 * @param {Object} deliveriesData - Deliveries Data
 * @return {Object} - The top 10 economical bowlers with their respective economy in the year 2015.
 */
function economicalBowlersOf2015(matchesData, deliveriesData) {
    let economicalBowlers = {}
    let matchIds = matchesData
        .filter(function (match) {
            return parseInt(match.season) === 2015
        })
        .map(match => parseInt(match.id));
    matchIds.filter((matchId) => {
        economicalBowlers = deliveriesData.reduce((accumulator, currentValue) => {
            let bowler = currentValue.bowler
            if (parseInt(matchId) === parseInt(currentValue.match_id)) {
                if (accumulator.hasOwnProperty(bowler)) {
                    if (accumulator[bowler].hasOwnProperty("balls") && accumulator[bowler].hasOwnProperty("totalRuns")) {
                        if (parseInt(currentValue.wide_runs) === 0 && parseInt(currentValue.noball_runs) === 0) {
                            accumulator[bowler]["balls"] += 1
                            accumulator[bowler]["totalRuns"] += parseInt(currentValue.total_runs)
                        } else {
                            accumulator[bowler]["totalRuns"] += parseInt(currentValue.total_runs)
                        }
                    }
                } else {
                    accumulator[bowler] = {
                        "balls": 1,
                        "totalRuns": 0
                    }
                }
            }
            return accumulator;
        }, economicalBowlers)
    });
    let economy = {}
    for (let bowler in economicalBowlers) {
        let overs = economicalBowlers[bowler]["balls"] / 6
        let totalRuns = economicalBowlers[bowler]["totalRuns"]
        economy[bowler] = totalRuns / overs;
    }
    let economyArray = (Object.entries(economy).sort((a, b) => a[1] - b[1]).slice(0, 10));
    return Object.assign(...economyArray.map(([k, v]) => ({[k]: v})));
}

module.exports = economicalBowlersOf2015;