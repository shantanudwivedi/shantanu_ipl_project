/**
 * Calculate number of matches won per team per year in IPL.
 * @param {Object} matchesData - Matches Data
 * @returns {Object} - Number of matches won by each team in every season
 */
function noOfMatchesWon(matchesData) {
    const filteredMatchesData = matchesData.filter(element => element.winner !== "")
    return filteredMatchesData.reduce((accumulator, currentValue) => {
        if (accumulator.hasOwnProperty(currentValue.winner)) {
            if (accumulator[currentValue.winner].hasOwnProperty(currentValue.season))
                accumulator[currentValue.winner][currentValue.season] += 1
            else
                accumulator[currentValue.winner][currentValue.season] = 1
        } else {
            accumulator[currentValue.winner] = {}
            accumulator[currentValue.winner][currentValue.season] = 1
        }
        return accumulator;
    }, {});
}

module.exports = noOfMatchesWon;