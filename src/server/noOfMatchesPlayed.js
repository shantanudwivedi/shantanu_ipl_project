/**
 * Calculate the number of matches played per year for all the years in IPL
 * @param {Object} matchesData - Matches Data
 * @returns {Object} - Number of matches played per year
 */
function noOfMatchesPlayed(matchesData) {
    return matchesData.reduce((accumulator, currentValue) => {
        if (accumulator.hasOwnProperty(currentValue.season)) {
            accumulator[currentValue.season] += 1;
        } else {
            accumulator[currentValue.season] = 1;
        }
        return accumulator;
    }, {});
}

module.exports = noOfMatchesPlayed;