/**
 * Find a player who has won the highest number of Player of the Match awards for each season
 * @param {Object} matchesData - Matches Data
 * @return {Object} - The player who has won the highest number of Player of the Match awards per season.
 */
function mostPlayerOfMatchAwardsOfEachSeason(matchesData) {
    let seasonlyPlayerOfMatchAwards = matchesData.reduce((accumulator, currentValue) => {
        let season = currentValue.season;
        let playerOfMatch = currentValue.player_of_match;
        if (accumulator.hasOwnProperty(season)) {
            if (accumulator[season].hasOwnProperty(playerOfMatch)) {
                accumulator[season][playerOfMatch] += 1;
            } else {
                accumulator[season][playerOfMatch] = 1;
            }
        } else {
            accumulator[season] = {};
            accumulator[season][playerOfMatch] = 1;
        }
        return accumulator;
    }, {});
    let playerOfMatchAwardsData = Object.values(seasonlyPlayerOfMatchAwards);
    let mostPlayerOfMatchAwards = {};
    for (let i = 0; i < playerOfMatchAwardsData.length; i++) {
        let mostPlayerOfMatchAwardsPerSeason = Object.entries(playerOfMatchAwardsData[i]).sort((a, b) => b[1] - a[1]).slice(0, 1);
        Object.assign(mostPlayerOfMatchAwards, Object.assign(...mostPlayerOfMatchAwardsPerSeason.map(([k, v]) => ({[k]: v}))));
    }
    return mostPlayerOfMatchAwards;
}

module.exports = mostPlayerOfMatchAwardsOfEachSeason;