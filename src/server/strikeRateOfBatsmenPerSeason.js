/**
 * Find the strike rate of a batsman for each season.
 * @param {Object} matchesData - Matches Data
 * @param {Object} deliveriesData - Deliveries Data
 * @return {Object} - The strike rate of batsmen per season.
 */
function strikeRateOfBatsmenPerSeason(matchesData, deliveriesData) {
    let matchIds = matchesData.reduce((accumulator, currentValue) => {
        let season = currentValue.season;
        let matchId = parseInt(currentValue.id);
        if (accumulator.hasOwnProperty(season)) {
            accumulator[season].push(matchId);
        } else {
            accumulator[season] = [];
            accumulator[season].push(matchId);
        }
        return accumulator;
    }, {});
    let matchIdValues = Object.values(matchIds);
    let seasonValues = Object.keys(matchIds);
    let strikeRateOfBatsmanPerSeason = {};
    seasonValues.filter((season) => {
       matchIdValues.filter((matchIds) => {
           matchIds.filter((matchId) => {
                deliveriesData.reduce((accumulator, currentValue) => {
                    let batsmanName = currentValue.batsman;
                    let batsmanRuns = parseInt(currentValue.batsman_runs);
                    if (parseInt(matchId) === parseInt(currentValue.match_id)) {
                        if (accumulator.hasOwnProperty(season)) {
                            if (accumulator[season].hasOwnProperty(batsmanName)) {
                                accumulator[season][batsmanName]["batsmanRuns"] += batsmanRuns;
                                accumulator[season][batsmanName]["ballsFaced"] += 1;
                            } else {
                                accumulator[season][batsmanName] = {};
                                accumulator[season][batsmanName]["batsmanRuns"] = batsmanRuns;
                                accumulator[season][batsmanName]["ballsFaced"] = 1;
                            }
                        } else {
                            accumulator[season] = {};
                            accumulator[season][batsmanName] = {};
                            accumulator[season][batsmanName]["batsmanRuns"] = batsmanRuns;
                            accumulator[season][batsmanName]["ballsFaced"] = 1;
                        }
                    }
                    return accumulator;
                }, strikeRateOfBatsmanPerSeason)
           })
       });
    });
    console.log(strikeRateOfBatsmanPerSeason);
    // for (let i = 0; i < seasonValues.length; i++) {
    //     let season = seasonValues[i];
    //     for (let k = 0; k < matchIdValues[i].length; k++) {
    //         let matchIdOfSeason = matchIdValues[i][k];
    //         for (let j = 0; j < deliveriesData.length; j++) {
    //             let batsmanName = deliveriesData[j].batsman;
    //             let batsmanRuns = parseInt(deliveriesData[j].batsman_runs);
    //             if (parseInt(matchIdOfSeason) === parseInt(deliveriesData[j].match_id)) {
    //                 if (strikeRateOfBatsmanPerSeason.hasOwnProperty(season)) {
    //                     if (strikeRateOfBatsmanPerSeason[season].hasOwnProperty(batsmanName)) {
    //                         strikeRateOfBatsmanPerSeason[season][batsmanName]["batsmanRuns"] += batsmanRuns;
    //                         strikeRateOfBatsmanPerSeason[season][batsmanName]["ballsFaced"] += 1;
    //                     } else {
    //                         strikeRateOfBatsmanPerSeason[season][batsmanName] = {};
    //                         strikeRateOfBatsmanPerSeason[season][batsmanName]["batsmanRuns"] = batsmanRuns;
    //                         strikeRateOfBatsmanPerSeason[season][batsmanName]["ballsFaced"] = 1;
    //                     }
    //                 } else {
    //                     strikeRateOfBatsmanPerSeason[season] = {};
    //                     strikeRateOfBatsmanPerSeason[season][batsmanName] = {};
    //                     strikeRateOfBatsmanPerSeason[season][batsmanName]["batsmanRuns"] = batsmanRuns;
    //                     strikeRateOfBatsmanPerSeason[season][batsmanName]["ballsFaced"] = 1;
    //                 }
    //             }
    //         }
    //     }
    // }
    // console.log(strikeRateOfBatsmanPerSeason);
    let strikeRatePerSeason = {};
    let batsmanData = Object.values(strikeRateOfBatsmanPerSeason);
    for (let i = 0; i < seasonValues.length; i++) {
        let currentSeason = seasonValues[i];
        let batsmanNames = Object.keys(batsmanData[i]);
        let batsmanInfo = Object.values(batsmanData[i]);
        for (let j = 0; j < batsmanNames.length; j++) {
            let strikeRate = (batsmanInfo[j].batsmanRuns / batsmanInfo[j].ballsFaced) * 100;
            let batsmanName = batsmanNames[j];
            if (strikeRatePerSeason.hasOwnProperty(currentSeason)) {
                strikeRatePerSeason[currentSeason][batsmanName] = strikeRate;
            } else {
                strikeRatePerSeason[currentSeason] = {};
                strikeRatePerSeason[currentSeason][batsmanName] = strikeRate;
            }
        }
    }
    return strikeRatePerSeason;
}

module.exports = strikeRateOfBatsmenPerSeason;