let fs = require('fs');
let convertCsvToJson = require("../server/convertCsvToJson.js");


//For the input deliveries.csv file
let inputFilename = "deliveries"
let outputFilename = "formattedDeliveriesFile"
convertCsvToJson(inputFilename, outputFilename);


// For the input matches.csv file
inputFilename = "matches"
outputFilename = "formattedMatchesFile"
convertCsvToJson(inputFilename, outputFilename);


//Input file data in json format
const matchesData = require("../input/formattedMatchesFile.json");
const deliveriesData = require("../input/formattedDeliveriesFile.json");


//Questions are exported as functions
let noOfMatchesPlayed = require("../server/noOfMatchesPlayed.js");
let noOfMatchesWon = require("../server/noOfMatchesWon.js");
let extraRunsConcededIn2016 = require("../server/extraRunsConcededIn2016.js");
let economicalBowlersOf2015 = require("../server/economicalBowlersOf2015.js");
let teamsWhichWonTossAndMatch = require("../server/teamsWhichWonTossAndMatch.js");
let mostPlayerOfMatchAwardsOfEachSeason = require("../server/mostPlayerOfMatchAwardsOfEachSeason.js");
let strikeRateOfBatsmenPerSeason = require("../server/strikeRateOfBatsmenPerSeason.js");
let mostDismissedPlayer = require("../server/mostDismissedPlayer.js");
let bestEconomicalBowlerInSuperOver = require("../server/bestEconomicalBowlerInSuperOver.js");


// Matches by year
let matchesByYear = noOfMatchesPlayed(matchesData);
console.log(matchesByYear);
fs.writeFileSync("../public/output/noOfMatchesPlayed.json", JSON.stringify(matchesByYear), {overwrite: false});


// Matches won by team
let matchesWonByTeam = noOfMatchesWon(matchesData);
console.log(matchesWonByTeam);
fs.writeFileSync("../public/output/noOfMatchesWon.json", JSON.stringify(matchesWonByTeam), {overwrite: false});


// Extra conceded by every team in year 2016
let extraRunsPerTeams = extraRunsConcededIn2016(matchesData, deliveriesData);
console.log(extraRunsPerTeams);
fs.writeFileSync("../public/output/extraRunsConcededIn2016.json", JSON.stringify(extraRunsPerTeams), {overwrite: false});


//Economical Bowlers of 2015
let economicalBowlers = economicalBowlersOf2015(matchesData, deliveriesData);
console.log(economicalBowlers);
fs.writeFileSync("../public/output/economicalBowlersOf2015.json", JSON.stringify(economicalBowlers), {overwrite: false});


//Teams who toss and also the match
let teamsWhoWonTossAndMatch = teamsWhichWonTossAndMatch(matchesData);
console.log(teamsWhoWonTossAndMatch);
fs.writeFileSync("../public/output/teamsWhichWonTossAndMatch.json", JSON.stringify(teamsWhoWonTossAndMatch), {overwrite: false});


// Player awarded with most player of match award per season
let mostPlayerOfMatchAwardsPerSeason = mostPlayerOfMatchAwardsOfEachSeason(matchesData);
console.log(mostPlayerOfMatchAwardsPerSeason);
fs.writeFileSync("../public/output/mostPlayerOfMatchAwardsPerSeason.json", JSON.stringify(mostPlayerOfMatchAwardsPerSeason), {overwrite: false});


// Strike rate of batsmen per season
let strikeRatePerSeason = strikeRateOfBatsmenPerSeason(matchesData, deliveriesData);
console.log(strikeRatePerSeason);
fs.writeFileSync("../public/output/strikeRateOfBatsmenPerSeason.json", JSON.stringify(strikeRatePerSeason), {overwrite: false});


// Most dismissed player per season
let playersDismissedData = mostDismissedPlayer(deliveriesData);
console.log(mostDismissedPlayer);
fs.writeFileSync("../public/output/mostDismissedPlayer.json", JSON.stringify(playersDismissedData), {overwrite: false});


// Best economical bowlers in super overs
let bestEconomicalBowlersInSuperOver = bestEconomicalBowlerInSuperOver(deliveriesData);
console.log(bestEconomicalBowlersInSuperOver);
fs.writeFileSync("../public/output/bestEconomicalBowlerInSuperOver.json", JSON.stringify(bestEconomicalBowlersInSuperOver), {overwrite: false});