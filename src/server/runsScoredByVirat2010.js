/**
 * Calculate the total numbers of runs scored by Virat Kohli in 2010
 * @param {Object} matchesData - Matches Data
 * @param {Object} deliveriesData - Deliveries Data
 * @return {Object} - The total number of runs scored by Virat Kohli in 2010
 */
function runsScoredByVirat2010(matchesData, deliveriesData) {
    let runsScoredByVirat = {
        "V Kohli": 0
    }
    let matchIds = matchesData
        .filter(function (match) {
            return parseInt(match.season) === 2010
        })
        .map(match => parseInt(match.id));
    matchIds.map((matchId) => {
        runsScoredByVirat = deliveriesData.reduce((accumulator, currentValue) => {
            let runsScored = parseInt(currentValue.batsman_runs);
            let batsman = currentValue.batsman
            if (parseInt(matchId) === parseInt(currentValue.match_id)) {
                if (batsman === "V Kohli") {
                    accumulator["V Kohli"] += runsScored;
                }
            }
            return accumulator;
        }, runsScoredByVirat);
    });
    // for (let i = 0; i < matchIds.length; i++) {
    //     let matchId = matchIds[i];
    //     for (let j = 0; j < deliveriesData.length; j++) {
    //         let runsScored = parseInt(deliveriesData[j].batsman_runs);
    //         let batsman = deliveriesData[j].batsman
    //         if (parseInt(matchId) === parseInt(deliveriesData[j].match_id)) {
    //             if (batsman === "V Kohli") {
    //                 runsScoredByVirat["V Kohli"] += runsScored
    //             }
    //         }
    //     }
    // }
    // console.log(runsScoredByVirat);
    return runsScoredByVirat;
}

module.exports = runsScoredByVirat2010;