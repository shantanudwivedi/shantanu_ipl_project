/**
 * Find the bowler with the best economy in super overs.
 * @param {Object} deliveriesData - Deliveries Data
 * @return {Object} - The best economical bowler in super overs
 */
function bestEconomicalBowlerInSuperOver(deliveriesData) {
    let economyInSuperOver = deliveriesData.reduce((accumulator, currentValue) => {
        if (parseInt(currentValue.is_super_over) === 1) {
            let bowler = currentValue.bowler;
            if (accumulator.hasOwnProperty(bowler)) {
                if (accumulator[bowler].hasOwnProperty("balls") && accumulator[bowler].hasOwnProperty("totalRuns")) {
                    if (parseInt(currentValue.wide_runs) === 0 && parseInt(currentValue.noball_runs) === 0) {
                        accumulator[bowler]["balls"] += 1;
                        accumulator[bowler]["totalRuns"] += parseInt(currentValue.total_runs);
                    } else {
                        accumulator[bowler]["totalRuns"] += parseInt(currentValue.total_runs);
                    }
                }
            } else {
                accumulator[bowler] = {
                    "balls": 1,
                    "totalRuns": 0
                };
            }
        }
        return accumulator;
    }, {});
    let strikeRateInSuperOver = {};
    for (let bowler in economyInSuperOver) {
        let overs = economyInSuperOver[bowler]["balls"] / 6;
        let totalRuns = economyInSuperOver[bowler]["totalRuns"];
        strikeRateInSuperOver[bowler] = totalRuns / overs;
    }
    let economyArray = Object.entries(strikeRateInSuperOver).sort((a, b) => a[1] - b[1]).slice(0, 1);
    return Object.assign(...economyArray.map(([k, v]) => ({[k]: v})));
}

module.exports = bestEconomicalBowlerInSuperOver;