const fs = require("fs");


/**
 * Convert csv files to json files and store them in input dir
 * @param {string} inputFilename - Input filename
 * @param {string} outputFilename - Output filename
 */
function convertCsvToJson(inputFilename, outputFilename) {
    csv = fs.readFileSync(`../data/${inputFilename}.csv`)
    let array = csv.toString().split("\r");
    let result = [];
    let headers = array[0].split(",")
    for (let i = 1; i < array.length - 1; i++) {
        let obj = {}
        let str = array[i]
        let s = ''
        let flag = 0
        for (let ch of str) {
            if (ch === '"' && flag === 0) {
                flag = 1
            } else if (ch === '"' && flag === 1) flag = 0
            if (ch === ', ' && flag === 0) ch = '|'
            if (ch !== '"') s += ch
        }
        let properties = s.split(",")
        for (let j in headers) {
            if (properties[j].includes(", ")) {
                obj[headers[j]] = properties[j]
                    .split(", ").map(item => item.trim())
            } else obj[headers[j]] = properties[j]
        }
        result.push(obj)
    }
    let json = JSON.stringify(result);
    fs.writeFileSync(`../input/${outputFilename}.json`, json, {overwrite: false});
    console.log(`json file of ${inputFilename} csv file is generated`)
}

module.exports = convertCsvToJson;